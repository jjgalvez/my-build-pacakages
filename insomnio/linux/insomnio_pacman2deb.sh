#!/bin/sh

fpm \
-f \
-m "Jose Galvez <jose@cybergalvez.com>" \
-x .BUILDINFO \
-a amd64 \
--no-auto-depends \
-d python3 \
-d python3-PyQt5 \
-d python3-click \
-s pacman -t deb \
$@
